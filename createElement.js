/**
 * Nicer way of creating elements
 * 
 * Usage:
 *
 * node = create_element({
 *     tag: "a", // only mandatory param
 *     href: "#",
 *     events: {
 *         "click": click_handler
 *     },
 *     content: [
 *         "label ",
 *         { tag: "img", src: "img.jpg" }
 *     ]
 * });
 *
 * node = create_element({
 *     tag: "div",
 *     class: "container,
 *     content: previously_created_node
 * });
 *
 * add_style("style data"); // adds style to head of doc
 *
 * add_style({
 *     id: "style id",
 *     content: "style data"
 * }); // adds style to head of doc
 *
 * Notes:
 *
 * The object passed to the functions is modified
 *
 * Tags can be nested in the tag declaration as such: "li a"
 * This will create a nested element "<li><a></a></li>"
 *
 * Parameteres in the creation objects are applied to the outermost tag, except fopr content which is applied to the innermost
 *
 * Individial tags can be writen as such: tag#id.class1.class2?param=data
 * This will produce <tag id="id" class="class1 class2" param="data"></tag>
 * The order of these extra params is strictly enfored:
 * 0-1 ids "#id"
 * 0+ classes ".class"
 * 0+ uri encoded attributes "?attr=data%20with%20spaces"
 */

(() => {
    "use strict";

    // convert string in form of tag#id.class1.class2?attr1=data%20info into
    // <tag id="id" class="class1 class2" attr1="data info"></tag>
    const tag_to_node = (input) => {
        let data = input;

        let attributes = [];
        let classes = [];
        let ids = [];

        { // get custom attributes
            const split = data.split("?");

            while (split.length > 1) attributes.push(split.pop());

            data = split[0];
        }

        { // get classes
            const split = data.split(".");

            while (split.length > 1) classes.push(split.pop());

            data = split[0];
        }

        { // get id
            const split = data.split("#");

            if (split.length > 2) console.warn("too many ids in " + input);

            while (split.length > 1) ids.push(split.pop());

            data = split[0];
        }

        const element = document.createElement(data);

        if (ids.length > 0) element.id = ids.pop();

        while (classes.length > 0) element.classList.add(classes.pop());

        while (attributes.length > 0) {
            const split = attributes.pop().split("=");

            split.push("");

            const k = decodeURIComponent(split[0]);
            const v = decodeURIComponent(split[1]);

            element.setAttribute(k, v);
        }

        return element;
    };

    // convert string in format "outer middle#id inner" into 
    // <outer><middle id="id"><inner></inner></middle></outer>
    const tag_to_nodes = (tag) => {
        const split = tag.split(/\s+/);

        const elements = [];

        for (const t of split) {
            if (t) {
                elements.push(tag_to_node(t));
            }
        }

        const data = {
            outer: elements[0],
            inner: elements[elements.length - 1]
        };

        for (let i = elements.length - 1; i >= 1; i--) {
            elements[i - 1].appendChild(elements[i]);
        }

        return data;
    };

    const create_text_node = (text) => {
        return document.createTextNode(text);
    };

    const create_element = (params) => {
        // get params
        const tag = params.tag;
        const events = params.events;

        let content = params.content;

        delete params.tag;
        delete params.events;
        delete params.content;

        // create elements

        const elements = tag_to_nodes(tag);

        // add events

        for (const type in events) {
            let list = events[type];

            if (!Array.isArray(list)) list = [ list ];

            for (const event of list) {
                elements.outer.addEventListener(type, event);
            }
        }

        // add children

        if (typeof content === "undefined") content = [];

        if (!Array.isArray(content)) content = [content];

        for (const val of content) {
            elements.inner.appendChild(process_element(val));
        }

        // add params

        for (const key in params) {
            const value = params[key];

            if (typeof value === "string") {
                elements.outer.setAttribute(key, params[key]);
            } else {
                elements.outer[key] = params[key];
            }
        }

        const element = elements.outer;

        for (var i = elements.length - 1; i >= 0; i--) {
            element[i] = elements[i];
        }
        element.length = elements.length;
        element.outer = elements.outer;
        element.inner = elements.inner;

        return element;
    };

    const return_node = (node) => {
        return node;
    };

    const cases = {
        string: create_text_node,
        object: create_element,
        node: return_node
    };

    const process_element = (params) => {
        if (Array.isArray(params)) {
            const elements = [];

            for (const p of params) elements.push(process_element(p));

            return elements;
        }

        const type = (params instanceof HTMLElement)? "node": typeof params;

        const handler = cases[type];

        if (!handler) {
            throw ("Undefined handler for type " + type);
        }

        return handler(params);
    };

    const add_style = (params) => {
        if (typeof params === "string") params = { content: params };

        params.tag = "style";
        params.type = "text/css";

        const style = process_element(params);

        document.head.appendChild(style);
    }

    window.create_element = process_element;
    window.add_style = add_style;
})();
